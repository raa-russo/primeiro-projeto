export const testConfig = {
    environment: {
        html: {
            url: "http://localhost:3000"
        },
        dev: {
            url: "http://localhost:3333"
        }
    },
    options: {
        smokeThresholds: {       
            vus: 1, 
            duration: '1s', 
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.03']
            }        
        },
    
        loadThresholds: {
            vus: 20,         
            thresholds: {
                http_req_duration: ['p(95)<3000'],
                http_req_failed: ['rate<0.05']
            },
            stages: [
                {duration: '10s', target: 10},
                {duration: '30s', target: 20},
                {duration: '20s', target: 10},
                {duration: '0s', target: 20}
            ] 
        },  
        
        stressThresholds: {
            vus: 20,         
            thresholds: {
                http_req_duration: ['p(95)<5000'],
                http_req_failed: ['rate<0.05']
            },
            stages: [
                {duration: '10s', target: 10},
                {duration: '30s', target: 20},
                {duration: '40s', target: 10},
                {duration: '50s', target: 20},
                {duration: '40s', target: 10},
                {duration: '30s', target: 20},
                {duration: '20s', target: 10},
                {duration: '0s',  target: 20}
            ] 
        }  
    }     
}