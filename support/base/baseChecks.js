import { check } from 'k6';

export class BaseChecks {
    checkStatusCode(response, expectedStatus = 200) {
        check(response, {
            'status code check': (r) => r.status === expectedStatus,
        })
    };

    checkBodySide(response, expectedSize){
        check (response, {
            [`Body size is less or equal then ${expectedSize} bytes`]: (r) => r.body.length <= expectedSize,
        })
    }

    checkResponseTime(response, expectedTime){
        check (response, {
            [`Response time duration is less or equal then ${expectedTime}ms`]: (r) => r.timings.duration <= expectedTime,
        })
    }

    checkResponseBody(response, expectedBody){
        check (response, {
            [`Response body includes "${expectedBody}"`]: (r) => r.body.includes(expectedBody),
        })
    }
    
}