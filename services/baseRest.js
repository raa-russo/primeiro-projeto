import http from 'k6/http';
import { BaseService } from './baseService.js';

export class BaseRest extends BaseService {
    constructor(base_uri) {
        super(base_uri);
    }

    post(endpoint, body, headers = {}, params = {}) {
        let uri = this.base_uri + endpoint;
        let options = this.buildOptions(headers, params);
        return http.post(uri, JSON.stringify(body), options)  
    }

    get(endpoint, body, headers = {}, params = {}) {
        let uri = this.base_uri + endpoint;
        let options = this.queryOptions(headers, params);
        return http.get(uri, JSON.stringify(body), options)  
    }

    put(endpoint, body, headers = {}, params = {}) {
        let uri = this.base_uri + endpoint;
        let options = this.editOptions(headers, params);
        return http.put(uri, JSON.stringify(body), options)  
    }

    del(endpoint, body, headers = {}, params = {}) {
        let uri = this.base_uri + endpoint;
        let options = this.deleteOptions(headers, params, null);
        return http.del(uri, JSON.stringify(body), options)  
    }
    //requisicao post
    buildOptions(headers = {}, params = {}) {
        return {
            headers: Object.assign({'Content-Type': 'application/json'}, {'monitor': false}, headers),
            params: Object.assign({}, params)
        }
    }

     //requisicao get
     queryOptions(headers = {}, params = {}) {
        return {
            headers: Object.assign({'Content-Type': 'application/json'}, {'monitor': false}, headers),
            params: Object.assign({}, params)
        }
    }

     //requisicao put
     editOptions(headers = {}, params = {}) {
        return {
            headers: Object.assign({'Content-Type': 'application/json'}, {'monitor': false}, {'administrador': true}, headers),
            params: Object.assign({}, params)
        }
    }

     //requisicao delete
     deleteOptions(headers = {}, params = {}) {
        return {
            headers: Object.assign({'Content-Type': 'application/json'}, {'monitor': false}, {'administrador': true}, headers),
            params: Object.assign({}, params)
        }
    }

}