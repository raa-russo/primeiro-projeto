[![Badge ServeRest](https://img.shields.io/badge/API-ServeRest-green)](https://github.com/ServeRest/ServeRest/)
# Aqui teremos uma breve explicação sobre o repositorio. 
- [Challenge da Sprint 1](#challenge-da-sprint-1)
- [Challenge da Sprint 2](#challenge-da-sprint-2)
- [Challenge da Sprint 3](#challenge-da-sprint-3)
- [Challenge da Sprint 4](#challenge-da-sprint-4)
- [Challenge da Sprint 5](#challenge-da-sprint-5)
- [Challenge da Sprint 6](#challenge-da-sprint-6)


# Compass UOL
* A Compass UOL é uma plataforma digital de serviços financeiros do Grupo UOL, oferecendo conta digital, cartão de crédito, empréstimos e investimentos. 
* Proporciona uma experiência integrada para gerenciamento de finanças pessoais, com abertura de conta fácil, transações bancárias, controle de gastos e investimentos. 
* Parcerias com instituições financeiras renomadas ampliam as opções disponíveis.
# AWS
* O estágio AWS para QA é uma oportunidade na área de qualidade de software, focado em testes e garantia de qualidade dos serviços da Amazon Web Services. 
* Nós os estagiários trabalharemos em projetos desafiadores, aprendendo sobre os serviços da AWS e executando testes para identificar bugs e melhorar os processos. 
* Colaboraremos com as equipes multidisciplinares para garantir alta qualidade e confiabilidade nos produtos e serviços da AWS.
* Faremos uma Wiki, que é uma plataforma colaborativa que permite a criação e compartilhamento de conhecimento de forma acessível e organizada, facilitando a colaboração entre equipes e o registro de informações importantes.Aqui irei resumir o que esta sendo feito no repositorio.

# Este repositório, esta sendo feito para anotações diárias para o aprendizado sobre teste de software, um estágio, oferecido pela _Compass_.

## Challenge da Sprint 1: 
- Comunicação em projetos
- Como criar bons READMEs para repositórios versionados
- Matriz de Eisenhower
- Fundamentos de agilidade
- Organização em equipes
- O QA Ágil
- Fundamentos de Teste (back-end)
- Pirâmide de testes
- Myers e o princípio de Pareto
- Fundamentos de CyberSecurity

## _Clique no link abaixo para ser redirecionado a Sprint 1!_
* Sprint 01 -  https://gitlab.com/raa-russo/primeiro-projeto/-/tree/pb_sprint1?ref_type=heads

## Challenge da Sprint 2:
- User Stories & Issues.
- Conceitos básicos para testes de API.
- Testes Estáticos.
- Tipos de erros comuns.
- Validações em APIs.
- Boas práticas.
- Planejamento de testes.
- Cobertura de testes.
- Análise de testes.
- Mapas mentais e testes.
- Introdução ao Postman.
- Ferramentas de apoio ao QA.

## _Clique no link abaixo para ser redirecionado a Sprint a 2!_
* Sprint 02 -  https://gitlab.com/raa-russo/primeiro-projeto/-/tree/pb_sprint2?ref_type=heads 

## Challenge da Sprint 3:
- Planejamento e análise de testes;
- Planejamento de testes de APIs REST;
- Definition of Done, Definition of Ready & Acceptance Criteria;
- Gestão de Issues;
- Gestão de Testes;
- Execução e criação de testes: avançando no Postman.

## _Clique no link abaixo para ser redirecionado a Sprint a 3!_
* Sprint 03 -  https://gitlab.com/raa-russo/primeiro-projeto/-/tree/pb_sprint3?ref_type=heads

## Challenge da Sprint 4:
- Testes Exploratórios
- Linguagem de programação Javascript.
- ​​​​​TDD - Test Driven Development ou Desenvolvimento Guiado por Testes
- Bibliotecas como Mocha e Chai.

## _Clique no link abaixo para ser redirecionado a Sprint a 4!_
* Sprint 04 -  https://gitlab.com/raa-russo/primeiro-projeto/-/tree/pb_sprint4?ref_type=heads

## Challenge da Sprint 5:
- Conceitos sobre Performance Testing
- Tipos de testes de performance - frontend/backend e suas diferenças
- Teste estático e dinâmico
- Geração de carga, falhas comuns e planejamento
- Fundamentos  da medição de performance
- Introdução ao Jmeter

## _Clique no link abaixo para ser redirecionado a Sprint a 5!_
* Sprint 05 -  https://gitlab.com/raa-russo/primeiro-projeto/-/tree/sprint_05?ref_type=heads

## Challenge da Sprint 6:
- Primeiro contato com K6
- Thresholds
- Validações com K6
- K6 CLI

## _Clique no link abaixo para ser redirecionado a Sprint a 6!_
* Sprint 06 -  https://gitlab.com/raa-russo/primeiro-projeto/-/tree/sprint_06?ref_type=heads

## _Clique no link abaixo para ser redirecionado a Wiki!_
* WIKI -  https://gitlab.com/raa-russo/primeiro-projeto/-/wikis/home

 
![compass](/uploads/47ba88ada08c7042c1b0a2e073ccade6/compass.jpg)

# _Autor_

Rogério Antonio Augusto

https://www.linkedin.com/in/rog%C3%A9rio-augusto-14a34429/