import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import uuid from '../../data/dynamic/uuid.js';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray('Users', function () {
  const jsonData = JSON.parse(open('../../data/static/user.json'));  
  return jsonData.users; 
});

const payload = {
  nome: `${uuid.v4()}`,
  email: `${uuid.v4().substring(24)}@qa.com`,
  password: 'teste',
  administrador: 'true'
}

export function setup() {
  const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload) 
  
  baseChecks.checkStatusCode(res, 201);
  //baseChecks.checkResponseSize(res, 3237);  
 
   console.log('Cadastro realizado com sucesso! ')
  return {responseData : res.json() }
}

export default () => {
  let userIndex = __ITER % data.length;
  let user = data[userIndex];
  
  const urlRes = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, user);

  baseChecks.checkStatusCode(urlRes,200)
  baseChecks.checkResponseTime(urlRes, 3000)
  baseChecks.checkResponseBody(urlRes, "Login realizado com sucesso")
  baseChecks.checkBodySide(urlRes, 2000)   
 
  console.log('Realizando Login')
  const token = urlRes.json().authorization;
  globalThis.token = token;  

  //console.log(globalThis.token);

  sleep(1);  
};
export function teardown(responseData) {
  const userId = responseData.responseData._id;
  const res = baseRest.del(ENDPOINTS.USER_ENDPOINT + `/${userId}`);

  baseChecks.checkStatusCode(res, 200);

  console.log(`Teardown deletando o usuario com o ID ${userId}`)
  sleep(1);
}



//k6 run tests/login/uidSmokeTest.js


