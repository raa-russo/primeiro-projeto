import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.loadThresholds; 

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseCheck = new BaseChecks();


const data = new SharedArray('Users', function () {
  const jsonData = JSON.parse(open('../../data/dynamic/newUser.json')); 
  return jsonData;
});

export function setup() {
  let userId = [];
  
  for (let i = 0; i < data.length; i++) {
    const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, data[i])
    baseCheck.checkStatusCode(res, 201)
    userId.push(res.json()._id)
    sleep(0.1)
  }
  console.log('Cadastro realizado com sucesso')
  return userId;
}

export default function () {
  let userIndex = __ITER % data.length; 
  let user = { "email": `${data[userIndex].email}`, "password": "teste" };
  let res = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, user) 

  baseCheck.checkStatusCode(res,200)
  baseCheck.checkResponseTime(res, 3000)
  baseCheck.checkResponseBody(res, "Login realizado com sucesso")
  baseCheck.checkBodySide(res, 2000)  

  console.log('Login realizado com sucesso')
  
  sleep(0.1);
}

export function teardown(userId) {
  for (let i = 0; i < data.length; i++) {
    baseRest.del(ENDPOINTS.USER_ENDPOINT, userId[i]);

    console.log('Registro excluído com sucesso')
    sleep(0.1)
  }
}
//k6 run tests/login/fakerLoadTest.js