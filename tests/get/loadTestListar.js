import { sleep } from 'k6';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js'; 
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.loadThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();


export default () => {    
  
  const urlUsers = baseRest.get(ENDPOINTS.USER_ENDPOINT);    
  console.log('Listando usuários');  

  const urlProd = baseRest.get(ENDPOINTS.PRODUCTS_ENDPOINT);  
  console.log('Listando produtos');  

  const urlCar = baseRest.get(ENDPOINTS.CARTS_ENDPOINT); 
  console.log('Listando carrinhos'); 

  //checks de USUARIOS
  baseChecks.checkStatusCode(urlUsers, 200)
  baseChecks.checkResponseTime(urlUsers, 3000)
 
  //checks de PRODUTOS
  baseChecks.checkStatusCode(urlProd, 200)
  baseChecks.checkResponseTime(urlProd, 3000)
    
  //checks de CARRINHOS
  baseChecks.checkStatusCode(urlCar, 200)
  baseChecks.checkResponseTime(urlCar, 3000) 
  
  sleep(1);
};


//k6 run tests/get/loadTestListar.js