import { sleep } from 'k6';
import uuid from '../data/dynamic/uuid.js';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {
        "summary.html": htmlReport(data),
    };
}

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

export function setup() {
    
    const payload = {
        nome: `${uuid.v4()}`,
        email: `${uuid.v4().substring(24)}@qa.com`,
        password: 'teste',
        administrador: 'true'
    };
    const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload);
    const id = res.json()._id;
    baseChecks.checkStatusCode(res, 201);

    globalThis.token = id
    console.log('Cadastro realizado com sucesso! ')
    const userEmail = payload.email;
   
    const loginPayload = {
        email: userEmail,
        password: 'teste',
    };
    const urlRes = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, loginPayload);
    console.log('Realizando Login')
    baseChecks.checkStatusCode(res, 200);
    const token = urlRes.json().authorization;
    globalThis.token = token;    

    sleep(1);
}

export default function prod() {
    setup();    
    const payload = JSON.stringify({
        nome: `${uuid.v4()}`,
        preco: 10,
        descricao: 'teste',
        quantidade: 5
    });       
    
    const res = baseRest.post(ENDPOINTS.PRODUCTS_ENDPOINT, payload + {'Authorization': `${globalThis.token}`});       
    baseChecks.checkStatusCode(res, 201);
    console.log('Produto cadastrado com sucesso! ')
    const idProd = res.json().id;
    globalThis.prodId = idProd;  

    sleep(1);   
}

export function teardown() {
    setup();
    prod();        
    console.log('Deletar produto')
    const res = baseRest.del(ENDPOINTS.PRODUCTS_ENDPOINT + `${globalThis.prodId}` + 'Authorization' + `${globalThis.token}` );     
    baseChecks.checkStatusCode(res, 200);
    console.log(`Produto excluido com sucesso`)  
          
    sleep(1);
}

//k6 run tests/tests/test.js