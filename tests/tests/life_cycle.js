import { sleep } from 'k6';

// 1. init code
// (inicializa variaveis, define options, (VUs, duration, tresholds))
let counter = 1;

export function setup() {
    // 2. setup code (executa apenas 1 vez antes da função principal)
    console.log(`SETUP ${counter}`)
  }
  
  export default function () {
    // 3. VU code (Ponto de entrada dos VUs, onde realizam os testes/chamadas na APi)
    console.log(`FUNÇÃO PRINCIPAL - ${counter} VU=${__VU} INTER=${__ITER}`)
    counter += 1
    sleep(1)    
  }
  
  export function teardown(data) {
    // 4. teardown code (executa apenas 1x apos a execucao da funcao principal)
    console.log(`TEARDOWN - ${counter}`)
  }
  
  //k6 run tests/life_cycle.js