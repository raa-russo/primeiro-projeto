import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray('Users', function () {
  const jsonData = JSON.parse(open('../data/dynamic/newUser.json'));
  return jsonData;
});

const payload = {
  "nome": "Fulano da Silva",
  "email": "fulano11@qa.com",
  "password": "teste",
  "administrador": "true"
}

export function setup() {
  const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload);

  baseChecks.checkStatusCode(res, 201);  

  console.log('Cadastro realizado com sucesso! ')
  const idUsers = res.json()._id;   
  
  return { responseData: res.json() }
}

export default () => {
  let userIndex = __ITER % data.length;  
  let user = { "email": `${data[userIndex].email}`, "password": "teste" };
  const urlRes = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT,user);  
  console.log('Realizando Login')
  
  baseChecks.checkStatusCode(urlRes, 200);
  sleep(1);
};

export function teardown(responseData) {
  const userId = responseData.responseData._id;
  const res = baseRest.del(ENDPOINTS.USER_ENDPOINT + `/${userId}`);

  baseChecks.checkStatusCode(res, 200);

  console.log(`Teardown deletando o usuario com o ID ${userId}`)

}

//k6 run tests/tests/teste.js