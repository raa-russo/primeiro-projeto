import { sleep } from 'k6';
import ID_Prod from "../post/uidCadProd.js";
import Token from "../login/log.js";
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

export default function () {
  Token();  
  ID_Prod();
     
  const res = baseRest.del(ENDPOINTS.PRODUCTS_ENDPOINT + `${globalThis.prodId}` + 'Authorization' + `${globalThis.token}` );   

  baseChecks.checkStatusCode(res, 200);  

  console.log('Produto excluido com sucesso! ')     
   

  //console.log(res.body);
  //console.log(id)
  sleep(1);
}

//k6 run tests/delete/excluiProd.js