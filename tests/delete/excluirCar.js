import { sleep } from 'k6';
import Token from "../login/log.js";
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

export default function () {
  Token();    
     
  const res = baseRest.del(ENDPOINTS.CARTS_ENDPOINT + '/concluir-compra',null, {'Authorization': `${globalThis.token}`} );   

  baseChecks.checkStatusCode(res, 200);  

  console.log('Registro excluído com sucesso! ')  
     
  sleep(1);
}

//k6 run tests/carrinhos/excluirCar.js