import { sleep } from 'k6';
import ID_Prod from "./cadastrar.js";
import Token from "../login/loadTest.js";
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

export default function () {
  Token();  
  ID_Prod();
     
  const res = baseRest.del(ENDPOINTS.PRODUCTS_ENDPOINT + `${globalThis.prodId}` + `${globalThis.token}` );   

  baseChecks.checkStatusCode(res, 200);  

  console.log('Produto excluido com sucesso! ')    

  
  sleep(1);
}

//k6 run tests/deletarProd.js
