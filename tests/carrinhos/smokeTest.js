import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import faker from "https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js";

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray('Users', function () {
  const jsonData = JSON.parse(open('../../data/dynamic/newUser.json'));   
  return jsonData;   
});  
  const name = faker.name.findName();  
  const email = faker.internet.email(name);
  const pass = faker.internet.password();  

  const payload = {
    nome:  name,    
    email: email, 
    password: pass,
    administrador: 'true'
  } ;   

export function setup() {
  const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload)   
  baseChecks.checkStatusCode(res, 201); 
  console.log('Cadastro realizado com sucesso! '); 
    
  const load = {
    email: payload.email,
    password: 'teste'
  }    
  const urlRes = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, load);
  console.log('Realizando Login')
  const token = urlRes.json().authorization;
  globalThis.token = token;
  sleep(1);   
}
export default () => { 
  const userToken = globalThis.token; 
  const payload = JSON.stringify( 
    {
      "idProduto": "BeeJh5lz3k6kSIzA",
      "quantidade": 1
    },
    {
      "idProduto": "YaeJ455lz3k6kSIzA",
      "quantidade": 3
    } 
  );  
  const Headers = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': userToken 
    },
  };  
   
const res = baseRest.post(ENDPOINTS.CARTS_ENDPOINT, payload, Headers );  

    console.log('Carrinho criado com sucesso! ') 
        
    baseChecks.checkStatusCode(res, 201)
    baseChecks.checkResponseTime(res, 1000)
    baseChecks.checkResponseBody(res, 'Cadastro com sucesso')
    baseChecks.checkBodySide(res, 2000)  

    const idProd = res.json()._id;
    globalThis.id = idProd; 
   
sleep(1);
  
};

export function teardown() {  
  const userId = globalThis.id;  
  const res = baseRest.del(ENDPOINTS.CARTS_ENDPOINT, userId); 

  baseChecks.checkStatusCode(res, 200);
  
  console.log('Teardown deletando o carrinho')

}

//k6 run tests/carrinhos/smokeTest.js