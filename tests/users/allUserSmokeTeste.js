import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.smokeThresholds;

const baseRest = new BaseRest(testConfig.environment.html.url);
const baseChecks = new BaseChecks();

const data = new SharedArray('User', function () {
  const jsonData = JSON.parse(open('../../data/dynamic/newUser.json'));
  return jsonData;
});

export default function () {
  
  let userIndex = __ITER % data.length;

  let payload = {
    nome: `${data[userIndex].nome}`,
    email: `${data[userIndex].nome}${[__VU]}@teste.com.br`,
    password: "teste",
    administrador: data[userIndex].administrador
  }

  let res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload)

  baseChecks.checkStatusCode(res, 200)
  baseChecks.checkResponseTime(res, 1000)
  baseChecks.checkResponseBody(res, 'Cadastro realizado com sucesso')
  baseChecks.checkBodySide(res, 2000)
  sleep(1);
}

export function teardown() {
  const response = baseRest.get(ENDPOINTS.USER_ENDPOINT)
  const data = Object.assign(response.json().usuarios);

  for (let i = 0; i < data.length; i++) {
    baseRest.del(ENDPOINTS.USER_ENDPOINT, data[i]._id);
  }
}

//k6 run tests/users/allUserSmokeTeste.js