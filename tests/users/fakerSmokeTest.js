import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import faker from "https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js";

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray('Users', function () {
  const jsonData = JSON.parse(open('../../data/dynamic/newUser.json'));   
  return jsonData;   
});

faker.locale = 'pt_BR';

const payload = [];

for(let i = 1; i < 1; i++) {  
  const name = faker.name.findName();  
  const email = faker.internet.email(name);
  const pass = faker.internet.password();
  const adm =  true;

  payload.push({  
    nome:  name,    
    email: email, 
    password: pass,
    administrador: true
  });
} 

export function setup() {
  const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload) 
  
  baseChecks.checkStatusCode(res, 201);
 
   console.log('Cadastro realizado com sucesso! ')
  return {responseData : res.json() }
}
console.log(payload);
export default () => {
  let userIndex = __ITER % data.length;
  let user = data[userIndex];
  
  const urlRes = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, user);

  baseChecks.checkStatusCode(urlRes, 200)
  baseChecks.checkResponseTime(urlRes, 1000)
  baseChecks.checkResponseBody(urlRes, 'Cadastro realizado com sucesso')
  baseChecks.checkBodySide(urlRes, 2000) 
 
  console.log('Realizando Login')
  sleep(1);  
};

export function teardown(responseData) {  
  const userId = responseData.responseData._id;  
  const res = baseRest.del(ENDPOINTS.USER_ENDPOINT + `/${userId}`);

  baseChecks.checkStatusCode(res, 200);
  
  console.log(`Teardown deletando o usuario com o ID ${userId}`)

}

//k6 run tests/users/fakerSmokeTest.js