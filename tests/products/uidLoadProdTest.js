import { sleep } from 'k6';
import uuid from '../../data/dynamic/uuid.js';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.loadThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const payload = JSON.stringify({
  email: 'fulano@qa.com',
  password: 'teste'
})

export function setup() {
  const res = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, payload ); 
  
  baseChecks.checkStatusCode(res, 200); 
 
  console.log('Realizando Login') 
  const token = res.json().authorization;
  globalThis.token = token;
}

export default () => {    
  
  const payload = JSON.stringify({
    nome: `${uuid.v4()}`,    
    preco: 10,
    descricao: 'teste',
    quantidade: 5
  });
  
  const headers = {
    Authorization: globalThis.token
  };
  
  const res = baseRest.post(ENDPOINTS.PRODUCTS_ENDPOINT, payload, { headers }); 
  
  baseChecks.checkStatusCode(res, 201);  ;
  baseChecks.checkResponseTime(res, 300)
  baseChecks.checkResponseBody(res, 'Login realizado com sucesso')
  baseChecks.checkBodySide(res, 2000)  
  
  console.log('Produto cadastrado com sucesso! ')
   
  const idProd = res.json()._id;
  globalThis.prodId = idProd;    
  
  sleep(2);  
};

export function teardown() {
  const res = baseRest.del(ENDPOINTS.PRODUCTS_ENDPOINT + `/${globalThis.prodId}`,{ headers: { Authorization: globalThis.token } });

  baseChecks.checkStatusCode(res, 200);

  console.log('Produto excluído com sucesso!');
  
  sleep(1);
};
//k6 run tests/products/uidLoadProdTest.js