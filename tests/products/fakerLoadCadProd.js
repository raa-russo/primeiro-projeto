import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.loadThresholds; 

const baseRest = new BaseRest(testConfig.environment.html.url);
const baseCheck = new BaseChecks();

const data = new SharedArray('Products', function () {
    const jsonData = JSON.parse(open('../../data/dynamic/newProd.json')); 
    return jsonData;
});

export function setup() {
    baseRest.post(ENDPOINTS.USER_ENDPOINT,
         {
            nome: "russo",
            email: "russo@teste.com.br",
            password: "teste",
            administrador: "true"
        }
    );
    sleep(1);
    let res = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, 
        {
            email: "russo@teste.com.br",
            password: "teste"
        }
    );
    let authorizationToken = res.json().authorization;
    return authorizationToken;
};

export default function (authorizationToken) {
    let userIndex = __ITER % data.length;
    let payload = {
        nome: `${data[userIndex].nome} ${__VU}`,
        preco: `${data[userIndex].preco}`,
        descricao: `${data[userIndex].descricao}`,
        quantidade: 10
    }
    let res = baseRest.post(ENDPOINTS.PRODUCTS_ENDPOINT, payload, { authorization: authorizationToken })

    baseCheck.checkStatusCode(res, 201)
    baseCheck.checkResponseTime(res, 1000)
    baseCheck.checkResponseBody(res, "Cadastro realizado com sucesso")
    baseCheck.checkBodySide(res, 2000)
    sleep(1);
};

export function teardown(authorizationToken) {

    let response = baseRest.get(ENDPOINTS.PRODUCTS_ENDPOINT)
    let data = Object.assign(response.json().produtos);
    for (let i = 0; i < data.length; i++) {
        baseRest.del(ENDPOINTS.PRODUCTS_ENDPOINT, data[i]._id, { authorization: authorizationToken });
    }

    response = baseRest.get(ENDPOINTS.USER_ENDPOINT)
    data = Object.assign(response.json().usuarios);
    for (let i = 0; i < data.length; i++) {
        baseRest.del(ENDPOINTS.USER_ENDPOINT, data[i]._id);
    }
}
//k6 run tests/products/fakerLoadCadProd.js