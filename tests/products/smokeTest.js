import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {
        "summary.html": htmlReport(data),
    };
}

export const options = testConfig.options.smokeThresholds;
const baseRest = new BaseRest(testConfig.environment.html.url);
const baseChecks = new BaseChecks();

const dataProducts = new SharedArray('products', function () {
const jsonData = JSON.parse(open('../../data/static/prod.json'));
    return jsonData.product;
});
  const prodLoad = {    
    "nome": "Electronic Bronze Shirt",
    "preco": "578.00",
    "descricao": "Boston's most stabilizes active muscles",
    "quantidade": 10
  }

  const dataUsers = new SharedArray('Users', function () {
    const jsonData = JSON.parse(open('../../data/static/user.json'));
    return jsonData.users;
  });
  
  const payload = {
    "nome": "Fulano da Silva",
    "email": "Julano@qa.com",
    "password": "teste",
    "administrador": "true"
  }
  
  export function setup() {
    const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload)  
    baseChecks.checkStatusCode(res, 201);  
    console.log('Cadastro realizado com sucesso! ')  
    console.log(res.body)

    const load = {
        email: payload.email,
        password: 'teste'
    }
    const logUrl = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, load)
    const token = logUrl.json().authorization;
    console.log(logUrl.body)
    console.log('Cadastro realizado com sucesso! ')  

    const prodUrl = baseRest.post(ENDPOINTS.PRODUCTS_ENDPOINT, prodLoad);
    const idProd = prodUrl.json()._id;     
    console.log(prodUrl.body)
    console.log('produto cadastrado com sucesso! ')     

    const database = {
        'authorization': token,
        '_id': idProd
    }
    return database;
}

export default function (database) {

    const idload = {
        "produtos": [
            {
                "idProduto": database._id,
                "quantidade": 1
            }
        ]
    }

    const res = baseRest.del(ENDPOINTS.CARTS_ENDPOINT, idload, { authorization: database.authorization })
    console.log('produto cadastro com sucesso')
}

//k6 run tests/products/smokeTest.js