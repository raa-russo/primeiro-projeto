import { sleep } from 'k6';
import Token from "../login/log.js";
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

export default function () {
  Token();   
  
  const payload = JSON.stringify( 
      {
        "idProduto": "BeeJh5lz3k6kSIzA",
        "quantidade": 1
      },
      {
        "idProduto": "YaeJ455lz3k6kSIzA",
        "quantidade": 3
      } 
    );  
    const Headers = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${globalThis.token}`, 
      },
    };  
     
  const res = baseRest.post(ENDPOINTS.CARTS_ENDPOINT, payload, Headers );   
      console.log(res.body)
  baseChecks.checkStatusCode(res, 201);  
    
  console.log('Carrinho criado com sucesso! ')  
     
  sleep(1);
}

//k6 run tests/post/cadCar.js