import { sleep } from 'k6';
import uuid from '../../data/dynamic/uuid.js';
import Token from "../login/log.js";
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

export default function () {
  Token();

  const payload = JSON.stringify({
    nome: `${uuid.v4()}`,
    preco: 10,
    descricao: 'teste',
    quantidade: 5
  })

  const res = baseRest.post(ENDPOINTS.PRODUCTS_ENDPOINT, payload + { 'Authorization': `${globalThis.token}` });

  baseChecks.checkStatusCode(res, 201);
  baseChecks.checkResponseTime(res, 1000)
  baseChecks.checkResponseBody(res, "Cadastro realizado com sucesso")
  baseChecks.checkBodySide(res, 2000)

  console.log('Produto cadastrado com sucesso! ')

  const idProd = res.json()._id;
  globalThis.id = idProd;
  console.log(idProd);

  sleep(2);

}


//k6 run tests/post/uidCadProd.js