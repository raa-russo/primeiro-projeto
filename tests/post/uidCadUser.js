import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import uuid from '../../data/dynamic/uuid.js';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray('Users', function () {
  const jsonData = JSON.parse(open('../../data/dynamic/newUser.json'));  
  return jsonData; 
});

const payload = {
  nome: `${uuid.v4()}`,
  email: `${uuid.v4().substring(24)}@qa.com`,
  password: 'teste',
  administrador: 'true'
}

export default () => {
  const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload) 
  
  baseChecks.checkStatusCode(res, 201);  
  baseChecks.checkResponseTime(res, 1000)
  baseChecks.checkResponseBody(res, "Cadastro realizado com sucesso")
  baseChecks.checkBodySide(res, 2000)  
 
  console.log('Cadastro realizado com sucesso! ')
  
  const id = res.json()._id;
  globalThis.id = id;
//  console.log(globalThis.id);
    
  sleep(2);    
};

//k6 run tests/post/uidCadUser.js


