import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import uuid from '../../data/dynamic/uuid.js';
import { BaseRest, BaseChecks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseChecks();

const data = new SharedArray('Users', function () {
  const jsonData = JSON.parse(open('../../data/dynamic/newUser.json'));  
  return jsonData.users; 
});

const payload = {
  nome: `${uuid.v4()}`,
  email: `${uuid.v4().substring(24)}@qa.com`,
  password: 'teste',
  administrador: 'true'
}

export function setup() {
  const res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload) 
  
  baseChecks.checkStatusCode(res, 201);   
 
   console.log('Cadastro realizado com sucesso! ')
  return {responseData : res.json() }
}

export default () => {
  let userIndex = __ITER % data.length;
  let user = data[userIndex];
  
  const urlRes = baseRest.post(ENDPOINTS.LOGIN_ENDPOINT, user);

  baseChecks.checkStatusCode(urlRes, 200);    
 
  console.log('Realizando Login')
  const token = urlRes.json().authorization;
  globalThis.token = token;    

  sleep(1);  
};

export function teardown(responseData) {  
  const userId = responseData.responseData._id;  

  const payload = JSON.stringify({
    nome: `${uuid.v4()}`,
    preco: 10,
    descricao: 'teste',
    quantidade: 5
  })
  
  const res = baseRest.post(ENDPOINTS.PRODUCTS_ENDPOINT, payload + `${globalThis.token}`);

  baseChecks.checkStatusCode(res, 200); 
  const prodId = res.responseData._id;  
  
  console.log(`Teardown cadastrando produto com o id ${prodId}`)

}


//k6 run tests/products/uidCadProd.js


