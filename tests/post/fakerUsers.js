import { sleep } from 'k6';
import { SharedArray } from 'k6/data';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
  return {
    "summary.html": htmlReport(data),
  };
}

export const options = testConfig.options.smokeThresholds; 

const base_uri = testConfig.environment.html.url;
const baseRest = new BaseRest(base_uri);
const baseCheck = new BaseChecks();

const data = new SharedArray('User', function () {
  const jsonData = JSON.parse(open('../../data/dynamic/newUser.json'));  
  return jsonData;
 
});

export default function () {
  let userIndex = __ITER % data.length;
  console.log(userIndex);
  let payload = {
    nome: `${data[userIndex].nome}`,
    email: `${data[userIndex].nome}${[__VU]}@teste.com.br`,
    password: "teste",
    administrador: data[userIndex].administrador
  }
  
  let res = baseRest.post(ENDPOINTS.USER_ENDPOINT, payload)

  baseCheck.checkStatusCode(res, 201)
  baseCheck.checkResponseTime(res, 300)
  baseCheck.checkResponseBody(res, "Cadastro realizado com sucesso")
  baseCheck.checkBodySide(res, 2000)

  console.log('cadastrado com sucesso')
  
  sleep(1);
}

export function teardown() {

  const response = baseRest.get(ENDPOINTS.USER_ENDPOINT)
  const data = Object.assign(response.json().usuarios);

  for (let i = 0; i < data.length; i++) {
    baseRest.del(ENDPOINTS.USER_ENDPOINT, data[i]._id);
  }    
}
//k6 run tests/post/fakerUsers.js