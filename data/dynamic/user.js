import { faker } from '@faker-js/faker'
import { writeFileSync } from 'fs';

function criarUsuario() {
  const listaUsuarios = [];

  for(let i = 1; i < 5; i++) {  
    const name = faker.person.fullName(); 
    const email = faker.internet.email();
    const pass = faker.internet.password();

    listaUsuarios.push({       
      email: email, 
      password: pass,        
    });
  }

try {
  writeFileSync('./data/dynamic/user.json', JSON.stringify(listaUsuarios));  
} catch (err) {
  console.error(err);
}
  console.log(listaUsuarios);
}


criarUsuario();

//node data/dynamic/user.js