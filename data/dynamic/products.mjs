import { faker } from "@faker-js/faker";
import * as fs from "fs"

const quant = 10;

function createRandomProducts(Array) {
    let randomName = faker.commerce.productName();
    let randomPrice = faker.commerce.price();
    let randomDescription = faker.commerce.productDescription();

    return {
        nome: randomName,
        preco: randomPrice,
        descricao: randomDescription,
        quantidade: 10
    };
}

let productsArray = [];

for (let i = 0; i <= quant; i++) {
    let product = createRandomProducts(productsArray);

    if (!productsArray.some(productInArray => productInArray.nome === product.nome)) { productsArray.push(product); }
    else { i = i - 1; }
}

fs.writeFile('./data/dynamic/newProd.json', JSON.stringify(productsArray, null, 2), 'utf8', (error) => {
    if (error) {
        console.error(error);
        throw error;
    }
    console.log('Products created');
});
