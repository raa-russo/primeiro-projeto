import { faker } from "@faker-js/faker";
import * as fs from "fs"

const quant = 10;
const admin = 1; 

function createRandomUsers() {
    let randomName = faker.person.firstName();
    let randomEmail = faker.internet.email([randomName]);     

    return {       
        email: randomEmail,
        password: 'senha',
      
    };
}

let users = [];

for (let i = 0; i <= quant; i++) {
    let user = createRandomUsers(users, admin);

    if (!users.some(userInArray => userInArray.email === user.email)) { users.push(user); }
    else { i = i - 1; }
}

fs.writeFile('./data/dynamic/newUser.json', JSON.stringify(users, null, 2), 'utf8', (error) => {
    if (error) {
        console.error(error);
        throw error;
    }
    console.log('Users created');
});
//node data/dynamic/users.mjs